package com.example.bill.im_here_app.Controller;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.CheckBox;
import android.widget.TextView;

import com.example.bill.im_here_app.R;
import com.example.bill.im_here_app.Tags.Tag;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Bill on 7/31/2016.
 */
public class CheckBoxesTagListAdapder extends ArrayAdapter {

    List<Tag> tagList;
    Context context;
    List<Integer> selectedList;
    public CheckBoxesTagListAdapder(Context context, int textViewResourceId, ArrayList<Tag> tagList) {
        super(context, textViewResourceId, tagList);
        this.tagList = tagList;
        this.context = context;
    }

    private class ViewHolder{
        CheckBox cb;
        TextView tv;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {

        ViewHolder holder = null;
        LayoutInflater layoutInflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        if(convertView == null) {
            convertView = layoutInflater.inflate(R.layout.tag_infor_layout, null);
            holder = new ViewHolder();
            holder.cb = (CheckBox) convertView.findViewById(R.id.tag_checkBox);
            holder.tv = (TextView)convertView.findViewById(R.id.describe_textview);
            convertView.setTag(holder);

            holder.cb.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if(v.isSelected()){
                        tagList.get(position).setSelected(false);
                        v.setSelected(false);
                    }
                    else {
                        tagList.get(position).setSelected(true);
                        v.setSelected(true);
                    }
                }
            });
        }
        else{
            holder = (ViewHolder) convertView.getTag();
        }
        holder.cb.setText(tagList.get(position).getTagName());
        if(tagList.get(position).isSelected())
            holder.cb.setChecked(true);
        holder.tv.setText(tagList.get(position).getTagDescripsion());

        return convertView;
    }
}
