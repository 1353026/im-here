package com.example.bill.im_here_app.Json;

import com.example.bill.im_here_app.Events.Event;
import com.example.bill.im_here_app.Tags.Tag;
import com.google.android.gms.maps.model.LatLng;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Bill on 8/11/2016.
 */
public class JsonFavoriteEventsParser {

    public static List<Event> getFavoriteEventsFromJSON(String jsonStr, List<String> favoriteTags) throws JSONException {

        List<Event> eventList = new ArrayList<>();

        int counter;

        final String COUNTER = "counter";
        final String EVENT_TITLE = "title";
        final String EVENT_ADDRESS = "address";
        final String EVENT_LATITUDE_LOCATION = "latitude_location";
        final String EVENT_LONGTITUDE_LOCATION = "longtitude_location";
        final String EVENT_POSTTIME = "post_time";
        final String EVENT_ENTRY_TIME = "event_time";
        final String EVENT_DURATION = "duration";
        final String EVENT_TAG = "event_tag";
        final String EVENT_OWNER = "owner";


        String idPrefix = "e";

        JSONObject events = new JSONObject(jsonStr);
        counter = events.getInt(COUNTER);


        for(int i = 1; i <= counter; i++){
            if(i <= 9)
                idPrefix += "0";

            JSONObject jsEvent =  events.getJSONObject(idPrefix+Integer.toString(i));

            String eventTag = jsEvent.getString(EVENT_TAG);

            for(int j = 0; j < favoriteTags.size(); j++){
                if(favoriteTags.get(j).equals(eventTag)){
                    String address;
                    String title;
                    double latitude;
                    double longtitude;
                    String postTimeStr;
                    String entryTimeStr;
                    String duration;
                    String tag;
                    String owner;

                    tag = favoriteTags.get(j);
                    address = jsEvent.getString(EVENT_ADDRESS);
                    title = jsEvent.getString(EVENT_TITLE);
                    latitude = jsEvent.getDouble(EVENT_LATITUDE_LOCATION);
                    longtitude = jsEvent.getDouble(EVENT_LONGTITUDE_LOCATION);
                    LatLng location = new LatLng(latitude, longtitude);
                    duration = jsEvent.getString(EVENT_DURATION);
                    postTimeStr = jsEvent.getString(EVENT_POSTTIME);
                    entryTimeStr = jsEvent.getString(EVENT_ENTRY_TIME);
                    owner = jsEvent.getString(EVENT_OWNER);

                    DateFormat dateFormat = new SimpleDateFormat("dd-MM-yyy HH:mm:ss");

                    try {
                            eventList.add(new Event(title, tag, duration, dateFormat.parse(postTimeStr) , dateFormat.parse(entryTimeStr), location, address, owner));
                    } catch (ParseException e) {
                        e.printStackTrace();
                    }
                }else{
                    continue;
                }

            }
            idPrefix = "e";
        }

        return  eventList;
    }
}
