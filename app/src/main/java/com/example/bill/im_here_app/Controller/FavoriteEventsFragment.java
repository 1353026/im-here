package com.example.bill.im_here_app.Controller;

import android.app.ProgressDialog;
import android.content.Context;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.TextView;

import com.example.bill.im_here_app.Connection.HttpConnection;
import com.example.bill.im_here_app.Events.Event;
import com.example.bill.im_here_app.Json.JsonFavoriteEventsParser;
import com.example.bill.im_here_app.Json.JsonTagsParser;
import com.example.bill.im_here_app.R;
import com.example.bill.im_here_app.Tags.Tag;
import com.google.firebase.auth.FirebaseAuth;

import org.json.JSONException;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Bill on 8/7/2016.
 */
public class FavoriteEventsFragment extends Fragment{

    private View rootView = null;
    private ListView listView = null;
    private Context mContext = null;
    FirebaseAuth mAuth;
    String userID;

    ListAdapter eventListAdapter;
    List<Event> eventList;

    FetchTagsTask fetchTagsTask;

    ProgressDialog mProgressDialog;

    public FavoriteEventsFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        rootView = inflater.inflate(R.layout.fragment_favorite_events, container, false);




        return rootView;
    }

    @Override
    public void onStart() {
        super.onStart();
        mContext = this.getActivity();
        fetchTagsTask = new FetchTagsTask();
        mProgressDialog = ProgressDialog.show(mContext,"Favorite events", "Loading...");
        fetchTagsTask.execute();
    }

    public class FetchTagsTask extends AsyncTask<Void, Void, List<String>> {
        @Override
        protected List<String> doInBackground(Void...params) {
            try{
                //Todo: testing load tags by hardcode userID


                mAuth = FirebaseAuth.getInstance();
                userID  = mAuth.getCurrentUser().getUid();
//                userID = PreferenceManager.getDefaultSharedPreferences(getActivity().getApplicationContext()).getString("UID","");

                List<String> res = new ArrayList<>();
                res.add(HttpConnection.getAllEventsJSONDataBufferByHttp());
                res.add(HttpConnection.getFavoriteTagsJSONDataBufferByHttp(userID));
                return res;
            }catch (Exception e){
                e.printStackTrace();
            }

            return null;
        }


        @Override
        protected void onPostExecute(List<String> s) {
            if(s != null){
                try {
                    //get all tag
                    List<String> eventFavoriteTags = JsonTagsParser.getFavoriteTagListFromUser(s.get(1));

                    eventList = JsonFavoriteEventsParser.getFavoriteEventsFromJSON(s.get(0),eventFavoriteTags);

                    //sort by the newest event

                    eventListAdapter = new FavoriteEventListAdapter(getActivity(), R.layout.favorite_events_infor, (ArrayList<Event>)eventList);
                    listView = (ListView) rootView.findViewById(R.id.favorite_events_listview);
                    listView.setAdapter(eventListAdapter);
                    listView.setItemsCanFocus(true);

                    listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                        @Override
                        public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

                        }
                    });

                } catch (JSONException e) {
                    e.printStackTrace();
                }finally {
                    mProgressDialog.dismiss();
                }
            }
        }

    }
}
