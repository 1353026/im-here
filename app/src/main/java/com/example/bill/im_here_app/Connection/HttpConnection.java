package com.example.bill.im_here_app.Connection;

import android.net.Uri;
import android.util.Log;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.ProtocolException;
import java.net.URL;

/**
 * Created by Bill on 7/31/2016.
 */
public class HttpConnection {
    static HttpURLConnection urlConnection;
    static BufferedReader reader;

    private static final String BASE_URL = "https://im-here-firebase.firebaseio.com";

    private static final String DATA_PACKAGE_TYPE = ".json";

    static String apiID = "9d29b0c3e1207f0fabc8abfb63ec07cd";

    public HttpConnection() {
        urlConnection = null;
        reader = null;
    }

    public static String GetTagJSONDataBufferByHttp() throws IOException {
        try{


            //Construct the URL for thetagsdb

            Uri builder = Uri.parse(BASE_URL + "/tags" + DATA_PACKAGE_TYPE);


            URL url = new URL(builder.toString());

            //request:
            urlConnection = (HttpURLConnection) url.openConnection();
            urlConnection.setRequestMethod("GET");
            urlConnection.connect();

            //Read input Stream into Data
            InputStream inputStream = urlConnection.getInputStream();
            StringBuffer buffer = new StringBuffer();
            if(inputStream == null){
                return null;
            }

            reader = new BufferedReader(new InputStreamReader(inputStream));
            String line;
            while ((line = reader.readLine()) != null){
                //make easier when debug
                buffer.append(line + "\n");
            }

            if(buffer.length() == 0){
                return null;
            }

            return buffer.toString();


        } catch (MalformedURLException e) {
            Log.e("TAGS GET REQUEST", "Error", e);
        } catch (ProtocolException e) {
            Log.e("TAGS GET REQUEST", "Error", e);
        } catch (IOException e) {
            Log.e("TAGS GET REQUEST", "Error", e);
        }finally {
            if (urlConnection != null){
                urlConnection.disconnect();

            }
            if(reader != null) {
                reader.close();
            }
        }
        return "";

    }

    public static String GetSelectedTagsJSONDataBufferByHttp(String userId) throws IOException {
        try{


            //Construct the URL for thetagsdb

            Uri builder = Uri.parse(BASE_URL+ "/users/" + userId + DATA_PACKAGE_TYPE);


            URL url = new URL(builder.toString());

            //request:
            urlConnection = (HttpURLConnection) url.openConnection();
            urlConnection.setRequestMethod("GET");
            urlConnection.connect();

            //Read input Stream into Data
            InputStream inputStream = urlConnection.getInputStream();
            StringBuffer buffer = new StringBuffer();
            if(inputStream == null){
                return null;
            }

            reader = new BufferedReader(new InputStreamReader(inputStream));
            String line;
            while ((line = reader.readLine()) != null){
                //make easier when debug
                buffer.append(line + "\n");
            }

            if(buffer.length() == 0){
                return null;
            }

            return buffer.toString();


        } catch (IOException e) {
            Log.e("TAGS GET REQUEST", "Error", e);
        }finally {
            if (urlConnection != null){
                urlConnection.disconnect();

            }
            if(reader != null) {
                reader.close();
            }
        }
        return "";
    }

    public static String getFavoriteTagsJSONDataBufferByHttp(String userId){
        try{
            Uri builder = Uri.parse(BASE_URL+ "/users/" + userId + "/tags/" + DATA_PACKAGE_TYPE);


            URL url = new URL(builder.toString());

            //request:
            urlConnection = (HttpURLConnection) url.openConnection();
            urlConnection.setRequestMethod("GET");
            urlConnection.connect();

            //Read input Stream into Data
            InputStream inputStream = urlConnection.getInputStream();
            StringBuffer buffer = new StringBuffer();
            if(inputStream == null){
                return null;
            }

            reader = new BufferedReader(new InputStreamReader(inputStream));
            String line;
            while ((line = reader.readLine()) != null){
                //make easier when debug
                buffer.append(line + "\n");
            }

            if(buffer.length() == 0){
                return null;
            }

            return buffer.toString();
        }catch (Exception e){
            e.printStackTrace();
        }finally {
            if (urlConnection != null){
                urlConnection.disconnect();

            }
            if(reader != null) {
                try {
                    reader.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
        return "";
    }

    public static String getAllEventsJSONDataBufferByHttp(){
        try{
            Uri builder = Uri.parse(BASE_URL + "/event/" + DATA_PACKAGE_TYPE);


            URL url = new URL(builder.toString());

            //request:
            urlConnection = (HttpURLConnection) url.openConnection();
            urlConnection.setRequestMethod("GET");
            urlConnection.connect();

            //Read input Stream into Data
            InputStream inputStream = urlConnection.getInputStream();
            StringBuffer buffer = new StringBuffer();
            if(inputStream == null){
                return null;
            }

            reader = new BufferedReader(new InputStreamReader(inputStream));
            String line;
            while ((line = reader.readLine()) != null){
                //make easier when debug
                buffer.append(line + "\n");
            }

            if(buffer.length() == 0){
                return null;
            }

            return buffer.toString();
        }catch (Exception e){
            e.printStackTrace();
        }finally {
            if (urlConnection != null){
                urlConnection.disconnect();

            }
            if(reader != null) {
                try {
                    reader.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
        return "";
    }

    public static String getJoiningEventsJSONDataBufferByHttp(String userId){
        try{
            Uri builder = Uri.parse(BASE_URL + "/users/" + userId + "/joining_events/" + DATA_PACKAGE_TYPE);


            URL url = new URL(builder.toString());

            //request:
            urlConnection = (HttpURLConnection) url.openConnection();
            urlConnection.setRequestMethod("GET");
            urlConnection.connect();

            //Read input Stream into Data
            InputStream inputStream = urlConnection.getInputStream();
            StringBuffer buffer = new StringBuffer();
            if(inputStream == null){
                return null;
            }

            reader = new BufferedReader(new InputStreamReader(inputStream));
            String line;
            while ((line = reader.readLine()) != null){
                //make easier when debug
                buffer.append(line + "\n");
            }

            if(buffer.length() == 0){
                return null;
            }

            return buffer.toString();
        }catch (Exception e){
            e.printStackTrace();
        }finally {
            if (urlConnection != null){
                urlConnection.disconnect();

            }
            if(reader != null) {
                try {
                    reader.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
        return "";
    }

    public static String getEventCounterJSONDataBufferByHttp(){
        try{
            Uri builder = Uri.parse(BASE_URL + "/event/counter" + DATA_PACKAGE_TYPE);


            URL url = new URL(builder.toString());

            //request:
            urlConnection = (HttpURLConnection) url.openConnection();
            urlConnection.setRequestMethod("GET");
            urlConnection.connect();

            //Read input Stream into Data
            InputStream inputStream = urlConnection.getInputStream();
            StringBuffer buffer = new StringBuffer();
            if(inputStream == null){
                return null;
            }

            reader = new BufferedReader(new InputStreamReader(inputStream));
            String line;
            while ((line = reader.readLine()) != null){
                //make easier when debug
                buffer.append(line + "\n");
            }

            if(buffer.length() == 0){
                return null;
            }

            return buffer.toString();
        }catch (Exception e){
            e.printStackTrace();
        }finally {
            if (urlConnection != null){
                urlConnection.disconnect();

            }
            if(reader != null) {
                try {
                    reader.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
        return "";
    }
}
