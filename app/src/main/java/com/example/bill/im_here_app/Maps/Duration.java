package com.example.bill.im_here_app.Maps;

/**
 * Created by Bill on 8/6/2016.
 */
public class Duration {
    public String text;
    public int value;

    public Duration(String text, int value) {
        this.text = text;
        this.value = value;
    }
}
