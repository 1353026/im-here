package com.example.bill.im_here_app;

import android.app.DatePickerDialog;
import android.app.ProgressDialog;
import android.app.TimePickerDialog;
import android.location.Location;
import android.os.AsyncTask;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.TimePicker;

import com.example.bill.im_here_app.Connection.HttpConnection;
import com.example.bill.im_here_app.Controller.FavoriteEventListAdapter;
import com.example.bill.im_here_app.DateTime.DateTimeConverter;
import com.example.bill.im_here_app.Events.Event;
import com.example.bill.im_here_app.Json.JsonEventsParser;
import com.example.bill.im_here_app.Json.JsonTagsParser;
import com.example.bill.im_here_app.User.User;
import com.google.android.gms.maps.model.LatLng;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

import org.json.JSONException;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class PostingAnEventActivity extends AppCompatActivity {

    DatabaseReference mDatabase;
    private FirebaseAuth mAuth;
    String userID;

    EditText txtTitle;
    EditText txtAddress;
    EditText txtLatitude;
    EditText txtLongitude;
    EditText txtDuration;
    EditText txtEventTag;
    EditText txtDateEntry;
    EditText txtTimeEntry;

    Button btnSelectDate;
    Button btnSelectTime;

    int mYear, mMonth, mDay, mHour, mMinute;

    int curEventCounter;

    FetchCounterTask mGetCounterTask;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_posting_an_event);
        mDatabase = FirebaseDatabase.getInstance().getReference();
        mAuth = FirebaseAuth.getInstance();

        userID  = mAuth.getCurrentUser().getUid();

        txtTitle = (EditText)findViewById(R.id.title_editText);
        txtAddress = (EditText)findViewById(R.id.addreess_editText);
        txtLatitude = (EditText)findViewById(R.id.latitude_editText);
        txtLongitude = (EditText)findViewById(R.id.longitude_editText);
        txtDuration = (EditText)findViewById(R.id.duration_editText);
        txtEventTag  = (EditText)findViewById(R.id.event_tag_editText);
        txtDateEntry = (EditText)findViewById(R.id.entry_date_editText);
        txtTimeEntry = (EditText)findViewById(R.id.entry_time_editText);

        btnSelectDate = (Button) findViewById(R.id.select_date_button);
        btnSelectTime = (Button) findViewById(R.id.select_time_button);

    }

    @Override
    protected void onResume() {
        super.onResume();
        mGetCounterTask = new FetchCounterTask();
        mGetCounterTask.execute();
    }

    public void onPostClick(View v){
        if(v.getId() == R.id.post_button){
            if(isFullFillTheInputData()){

//                ProgressDialog mProgress = ProgressDialog.show(this, "Posting","Please wait...", true);


                String eventTitle = txtTitle.getText().toString();
                String address = txtAddress.getText().toString();
                Double lat = Double.parseDouble(txtLatitude.getText().toString());
                Double longi = Double.parseDouble(txtLongitude.getText().toString());
                String entryDay = txtDateEntry.getText().toString();
                String entryTime = txtTimeEntry.getText().toString();
                String duration = txtDuration.getText().toString();
                String eventTag = txtEventTag.getText().toString();
                LatLng location = new LatLng(lat,longi);

                Date openTime = DateTimeConverter.String2DateTime(entryDay + " " + entryTime);

                Calendar c = Calendar.getInstance();
                Date postTime = c.getTime();
                Event newEvent = new Event(eventTitle,eventTag,duration,postTime,openTime,location,address,userID);

                curEventCounter += 1;

                writeNewPost(newEvent);

//                mProgress.dismiss();
            }
        }
    }

    private boolean isFullFillTheInputData(){


        return true;
    }

    private void writeNewPost(Event newEvent) {
        // Create new post at /user-posts/$userid/$postid and at

        // /posts/$postid simultaneously
        String key = mDatabase.child("newEvent").push().getKey();
        Map<String, Object> postValues = newEvent.toMap();

        Map<String, Object> childUpdates = new HashMap<>();
        //childUpdates.put(key, postValues);
        childUpdates.put("/event/e0" + Integer.toString(curEventCounter) , postValues);

        Map eventCounter = new HashMap();
        eventCounter.put("/event/counter",curEventCounter);

        mDatabase.updateChildren(childUpdates);
        mDatabase.updateChildren(eventCounter);
    }

    public void onButtonClick(View v){
        int id = v.getId();
        if(id == R.id.select_date_button){
            final Calendar c = Calendar.getInstance();
            mYear = c.get(Calendar.YEAR);
            mMonth = c.get(Calendar.MONTH);
            mDay = c.get(Calendar.DAY_OF_MONTH);


            DatePickerDialog datePickerDialog = new DatePickerDialog(this,
                    new DatePickerDialog.OnDateSetListener() {

                        @Override
                        public void onDateSet(DatePicker view, int year,
                                              int monthOfYear, int dayOfMonth) {

                            txtDateEntry.setText(dayOfMonth + "-" + (monthOfYear + 1) + "-" + year);

                        }
                    }, mYear, mMonth, mDay);
            datePickerDialog.show();
        }else if(id == R.id.select_time_button){
            // Get Current Time
            final Calendar c = Calendar.getInstance();
            mHour = c.get(Calendar.HOUR_OF_DAY);
            mMinute = c.get(Calendar.MINUTE);

            // Launch Time Picker Dialog
            TimePickerDialog timePickerDialog = new TimePickerDialog(this,
                    new TimePickerDialog.OnTimeSetListener() {

                        @Override
                        public void onTimeSet(TimePicker view, int hourOfDay,
                                              int minute) {

                            txtTimeEntry.setText(hourOfDay + ":" + minute);
                        }
                    }, mHour, mMinute, false);
            timePickerDialog.show();
        }
    }
    public class FetchCounterTask extends AsyncTask<Void, Void, String> {
        @Override
        protected String doInBackground(Void...params) {
            try{
                //get current event counter:

                String res = HttpConnection.getEventCounterJSONDataBufferByHttp();
                return res;
            }catch (Exception e){
                e.printStackTrace();
            }

            return null;
        }


        @Override
        protected void onPostExecute(String s) {
            if(s != null){
                //get all tag
                curEventCounter = JsonEventsParser.getEventCounter(s);
            }
        }

    }
}
