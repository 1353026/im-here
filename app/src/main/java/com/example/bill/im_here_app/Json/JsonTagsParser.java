package com.example.bill.im_here_app.Json;

import android.util.Log;
import android.widget.Toast;

import com.example.bill.im_here_app.Tags.Tag;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Bill on 7/31/2016.
 */
public class JsonTagsParser {
    public static List<Tag> getTagListFromJSON(String tagsJsonStr) throws JSONException {

        List<Tag> tagList = new ArrayList<>();

        final String TAGS_RESULT = "tags";
        final String TAG_TITLE = "tag";
        final String TAG_DESCRIPTION = "description";

        //JSONObject tagsJSON= new JSONObject(tagsJsonStr);
        JSONArray tagArr = new JSONArray(tagsJsonStr);

        for(int i = 0; i < tagArr.length(); i++){
            JSONObject tag = tagArr.getJSONObject(i);

            String tagTitle = tag.getString(TAG_TITLE);
            String tagDes = tag.getString(TAG_DESCRIPTION);

            Tag tmpTag = new Tag(tagTitle, tagDes);
            tagList.add(tmpTag);
        }

        return  tagList;
    }

    public static void combineWithSelectedTagsFromJSON(String userJsonStr, List<Tag> tagList) throws JSONException {
        final String LOG_TAG = "Get selected tags";
        final String SELECTED_TAGS_RESULT = "tags";
        final String IS_FIRSTTIME = "first_time";

        JSONObject userJSON= new JSONObject(userJsonStr);

        try {
            JSONArray selectedTags = userJSON.getJSONArray(SELECTED_TAGS_RESULT);

            for (int i = 0; i < selectedTags.length(); i++) {
                for (int j = 0; j < tagList.size(); j++) {
                    if (selectedTags.get(i).toString().equals(tagList.get(j).getTagName())) {
                        tagList.get(j).setSelected(true);
                        break;
                    }
                }
            }
        }catch (Exception e){
            e.printStackTrace();
        }
        finally {
            return;
        }
    }

    public static List<String> getFavoriteTagListFromUser(String userJsonStr) {
        final String LOG_TAG = "Get favorite tags";
        final String FAVORITE_TAGS_RESULT = "tags";
        List<String> favoriteTags = new ArrayList<>();
        try {

            JSONArray favoritesTagsArr = new JSONArray(userJsonStr);

            for (int i = 0; i < favoritesTagsArr.length(); i++) {
                favoriteTags.add((String) favoritesTagsArr.get(i));
            }
        }catch (Exception e){
            e.printStackTrace();
        }

        return  favoriteTags;
    }
}
