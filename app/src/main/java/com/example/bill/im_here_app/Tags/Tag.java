package com.example.bill.im_here_app.Tags;

/**
 * Created by Bill on 7/31/2016.
 */
public class Tag {
    public Tag(String tagName, String tagDescripsion) {
        this.tagName = tagName;
        this.tagDescripsion = tagDescripsion;
        this.isSelected = false;
    }

    public String getTagName() {
        return tagName;
    }

    public void setTagName(String tagName) {
        this.tagName = tagName;
    }

    public String getTagDescripsion() {
        return tagDescripsion;
    }

    public void setTagDescripsion(String tagDescripsion) {
        this.tagDescripsion = tagDescripsion;
    }

    private String tagName;
    private String tagDescripsion;

    public boolean isSelected() {
        return isSelected;
    }

    public void setSelected(boolean selected) {
        isSelected = selected;
    }

    private boolean isSelected;
}
