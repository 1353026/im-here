package com.example.bill.im_here_app.DateTime;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * Created by vdao5 on 8/23/2016.
 */
public class DateTimeConverter {

    public static long DateTime2Long(Date date){
        long res = -1;
        try{
            res = date.getTime();
        }catch (Exception e){
            e.printStackTrace();
        }
        return res;
    }

    public static String DateTime2String(Date date){
        String res = "";
        try {
            DateFormat df = new SimpleDateFormat("dd-MM-yyy HH:mm");
            res = df.format(date);
        }catch (Exception e){
            e.printStackTrace();
        }
        return  res;
    }

    public  static  Date String2DateTime(String dateStr){
        Date res = new Date();
        try {
            DateFormat df = new SimpleDateFormat("dd-MM-yyy HH:mm");
            res = df.parse(dateStr);
        }catch (Exception e){
            e.printStackTrace();
        }
        return  res;
    }
}
