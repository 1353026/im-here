package com.example.bill.im_here_app.Events;

import com.google.android.gms.maps.model.LatLng;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by Bill on 8/11/2016.
 */
public class Event {
    private String owner;
    private String title;
    private String tagName;
    private String duration;
    private Date postTime;
    private Date entryTime;
    private LatLng location;
    private String address;

    public Event(String title, String tagName, String duration, Date postTime, Date entryTime,
                 LatLng location, String address, String owner) {
        this.title = title;
        this.tagName = tagName;
        this.duration = duration;
        this.postTime = postTime;
        this.entryTime = entryTime;
        this.location = location;
        this.address = address;
        this.owner = owner;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getTagName() {
        return tagName;
    }

    public void setTagName(String tagName) {
        this.tagName = tagName;
    }

    public String getDuration() {
        return duration;
    }

    public void setDuration(String duration) {
        this.duration = duration;
    }

    public Date getPostTime() {
        return postTime;
    }

    public void setPostTime(Date postTime) {
        this.postTime = postTime;
    }

    public Date getEntryTime() {
        return entryTime;
    }

    public void setEntryTime(Date entryTime) {
        this.entryTime = entryTime;
    }

    public LatLng getLocation() {
        return location;
    }

    public void setLocation(LatLng location) {
        this.location = location;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public Map<String, Object> toMap(){
        HashMap<String, Object> result = new HashMap<>();
        result.put("title", this.title);
        result.put("event_tag", this.tagName);
        result.put("duration", this.duration);
        result.put("event_time", this.entryTime.getTime());
        result.put("latitude_location", this.location.latitude);
        result.put("longtidude_location", this.location.longitude);
        result.put("post_time", this.postTime.getTime());
        result.put("address", this.address);
        result.put("owner", this.owner);

        return result;
    }

    public String getOwner() {
        return owner;
    }

    public void setOwner(String owner) {
        this.owner = owner;
    }
}
