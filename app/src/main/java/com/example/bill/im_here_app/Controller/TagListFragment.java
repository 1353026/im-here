package com.example.bill.im_here_app.Controller;

import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.ListAdapter;
import android.widget.ListView;

import com.example.bill.im_here_app.Connection.HttpConnection;
import com.example.bill.im_here_app.FirstTimeCheckListActivity;
import com.example.bill.im_here_app.Json.JsonTagsParser;
import com.example.bill.im_here_app.MainActionActivity;
import com.example.bill.im_here_app.R;
import com.example.bill.im_here_app.Tags.Tag;
import com.example.bill.im_here_app.User.User;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

import org.json.JSONException;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by Bill on 7/31/2016.
 */
public class TagListFragment extends Fragment {

    private View rootView = null;
    private ListView listView = null;

    FirebaseAuth mAuth;
    DatabaseReference mDatabase;
    String userID;

    ListAdapter tagListAdapter;

    List<Tag> tagList;

    FetchTagsTask fetchTagsTask;

    @Override
    public void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        fetchTagsTask = new FetchTagsTask();



//        tagList = new ArrayList<>();

       ;
    }

    @Override
    public void onStart() {
        super.onStart();
        fetchTagsTask.execute();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        rootView =  inflater.inflate(R.layout.fragment_first_time_check_list, container, false);

        Button submitButton = (Button)rootView.findViewById(R.id.submit_tags_button);
        submitButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onClickButton(v);
                Intent actionIntetn = new Intent(getContext(), MainActionActivity.class);
                startActivity(actionIntetn);


            }
        });

        return rootView;
    }

    public class FetchTagsTask extends AsyncTask<Void, Void, List<String>>{
        @Override
        protected List<String> doInBackground(Void...params) {
            try{
                //Todo: testing load tags by hardcode userID
                mAuth = FirebaseAuth.getInstance();
                userID  = mAuth.getCurrentUser().getUid();
//                userID = PreferenceManager.getDefaultSharedPreferences(getActivity().getApplicationContext()).getString("UID","");

                List<String> res = new ArrayList<>();
                res.add(HttpConnection.GetTagJSONDataBufferByHttp());
                res.add(HttpConnection.GetSelectedTagsJSONDataBufferByHttp(userID));
                return res;
            }catch (Exception e){
                e.printStackTrace();
            }

            return null;
        }


        @Override
        protected void onPostExecute(List<String> s) {
            if(s != null){
                try {
                    //get all tag
                    tagList = JsonTagsParser.getTagListFromJSON(s.get(0));

                    //get all tag combined with selected tags
                    JsonTagsParser.combineWithSelectedTagsFromJSON(s.get(1), tagList);

                    tagListAdapter = new CheckBoxesTagListAdapder(getActivity(), R.layout.tag_infor_layout, (ArrayList<Tag>)tagList);
                    listView = (ListView) rootView.findViewById(R.id.tag_listView);
                    listView.setAdapter(tagListAdapter);
                    listView.setItemsCanFocus(true);

                    listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                        @Override
                        public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                            
                        }
                    });

                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }

    }

    private void writeNewPost(List<String> tags) {
        // Create new post at /user-posts/$userid/$postid and at
        // /posts/$postid simultaneously
        String key = mDatabase.child("users").push().getKey();
        User user = new User(userID, tags, false);
        Map<String, Object> postValues = user.toMap();

        Map<String, Object> childUpdates = new HashMap<>();
        //childUpdates.put(key, postValues);
        childUpdates.put("/users/" + userID , postValues);

        mDatabase.updateChildren(childUpdates);
    }


    public void onClickButton(View v){
        if(v.getId() == R.id.submit_tags_button){

            mDatabase = FirebaseDatabase.getInstance().getReference();

            List<String> tags = new ArrayList<>();
            for(int i = 0; i < tagList.size(); i++) {
                if(tagList.get(i).isSelected()){
                    tags.add(tagList.get(i).getTagName());
                }
            }
            writeNewPost(tags);
        }
    }
}
