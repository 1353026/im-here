package com.example.bill.im_here_app.Controller;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageButton;
import android.widget.TextView;

import com.example.bill.im_here_app.Events.Event;
import com.example.bill.im_here_app.MapsActivity;
import com.example.bill.im_here_app.R;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * Created by Bill on 8/9/2016.
 */
public class FavoriteEventListAdapter extends ArrayAdapter {
    List<Event> favoriteEventList;
    Context context;
    public FavoriteEventListAdapter(Context context, int resourceId, ArrayList<Event> eventList) {
        super(context, resourceId, eventList);
        this.favoriteEventList = eventList;
        this.context = context;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        LayoutInflater layoutInflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);

        if(convertView == null){
            convertView = layoutInflater.inflate(R.layout.favorite_events_infor, null);
        }

        ImageButton directionButton = (ImageButton) convertView.findViewById(R.id.map_direction_button);
        directionButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

            }
        });

        TextView eventTitle = (TextView)convertView.findViewById(R.id.event_name_textview);
        TextView timeEntry = (TextView)convertView.findViewById(R.id.event_entry_time_textview);
        ImageButton mapDirectionButton = (ImageButton)convertView.findViewById(R.id.map_direction_button);

        eventTitle.setText(favoriteEventList.get(position).getTitle());
        Date date = favoriteEventList.get(position).getEntryTime();
        DateFormat dateFormat = new SimpleDateFormat("dd-MM-yyy HH:mm:ss");
        String dateStr = dateFormat.format(date);
        timeEntry.setText(dateStr);

        mapDirectionButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent mapIntent = new Intent(context, MapsActivity.class);

                mapIntent.putExtra("Event_latitude_location",favoriteEventList.get(position).getLocation().latitude);
                mapIntent.putExtra("Event_longitude_location",favoriteEventList.get(position).getLocation().longitude);

                context.startActivity(mapIntent);
            }
        });

        return convertView;
    }

}
