package com.example.bill.im_here_app.User;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;

/**
 * Created by Bill on 8/4/2016.
 */
public class User {
    private String userEmail;
    private String userID;
    private boolean isFirstTime;

    private List<String> joiningEvents;
    private List<String> tags;

    public User(String userID, List<String> tags, List<String> joiningEvents ,boolean isFirstTime) {
        this.userID = userID;
        this.tags = tags;
        this.isFirstTime = isFirstTime;
        this.joiningEvents = joiningEvents;
    }

    public List<String> getTags() {
        return tags;
    }

    public void setTags(List<String> tags) {
        this.tags = tags;
    }

    public String getUserEmail() {
        return userEmail;
    }

    public void setUserEmail(String userEmail) {
        this.userEmail = userEmail;
    }

    public String getUserID() {
        return userID;
    }

    public void setUserID(String userID) {
        this.userID = userID;
    }


    public Map<String, Object> toMap(){
        HashMap<String, Object> result = new HashMap<>();
        result.put("user_email", userEmail);
        result.put("tags", tags);
        result.put("first_time", isFirstTime);

        return result;
    }

    public List<String> getJoiningEvents() {
        return joiningEvents;
    }

    public void setJoiningEvents(List<String> joiningEvents) {
        this.joiningEvents = joiningEvents;
    }
}
