package com.example.bill.im_here_app.Maps;

import java.util.List;

/**
 * Created by Bill on 8/6/2016.
 */
public interface DirectionFinderListener {
    void onDirectionFinderStart();
    void onDirectionFinderSuccess(List<Route> route);
}
