package com.example.bill.im_here_app.Json;

import com.example.bill.im_here_app.Events.Event;
import com.google.android.gms.maps.model.LatLng;

import org.json.JSONException;
import org.json.JSONObject;

import java.sql.Date;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Bill on 8/19/2016.
 */
public class JsonEventsParser {

    public static List<Event> getUpcomingEventsFromJsonStr(String jsonStr, List<String> eventIDs) throws JSONException {

        List<Event> eventList = new ArrayList<>();
        JSONObject events = new JSONObject(jsonStr);

        final String EVENT_TITLE = "title";
        final String EVENT_ADDRESS = "address";
        final String EVENT_LATITUDE_LOCATION = "latitude_location";
        final String EVENT_LONGTITUDE_LOCATION = "longtitude_location";
        final String EVENT_POSTTIME = "post_time";
        final String EVENT_ENTRY_TIME = "event_time";
        final String EVENT_DURATION = "duration";
        final String EVENT_TAG = "event_tag";
        final String EVENT_OWNER = "owner";

        String address;
        String title;
        double latitude;
        double longtitude;
        String postTimeStr;
        String entryTimeStr;
        String owner;
        String duration;
        String tag;

        for(int i= 0; i < eventIDs.size(); i++){

            JSONObject upComingEvent = events.getJSONObject(eventIDs.get(i));

            address = upComingEvent.getString(EVENT_ADDRESS);
            title = upComingEvent.getString(EVENT_TITLE);
            latitude = upComingEvent.getDouble(EVENT_LATITUDE_LOCATION);
            longtitude = upComingEvent.getDouble(EVENT_LONGTITUDE_LOCATION);
            duration = upComingEvent.getString(EVENT_DURATION);
            postTimeStr = upComingEvent.getString(EVENT_POSTTIME);
            entryTimeStr = upComingEvent.getString(EVENT_ENTRY_TIME);
            LatLng location = new LatLng(latitude, longtitude);
            tag = upComingEvent.getString(EVENT_TAG);
            owner = upComingEvent.getString(EVENT_OWNER);

            eventList.add(new Event(title, tag, duration, Date.valueOf(postTimeStr) , Date.valueOf(entryTimeStr), location, address, owner));

        }

        return  eventList;
    }

    public static int getEventCounter(String jsonStr){
        int res = -1;
        try{
            jsonStr = jsonStr.replace("\n","");
            res = Integer.parseInt(jsonStr);
        }catch(Exception e){
            e.printStackTrace();
        }
        return res;
    }
}
