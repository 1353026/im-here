package com.example.bill.im_here_app.Maps;

import com.google.android.gms.maps.model.LatLng;

import java.util.List;

/**
 * Created by Bill on 8/6/2016.
 */
public class Route {
    public Distance distance;
    public Duration duration;
    public String endAddress;
    public LatLng endLocation;
    public String startAddress;
    public LatLng startLocation;

    public List<LatLng> points;
}
